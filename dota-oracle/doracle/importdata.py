"""
denne filen er laget bare for å importere datasettet for å leke litt med det og se på dataene
"""

import codecs
import json
import csv
import re
import os
import glob

files = []
for file in os.listdir("C:/Users/Andre/dotadata/delvis"):
    if file.endswith(".json"):
        files.append(os.path.join("C:/Users/Andre/dotadata/delvis", file))
data = []

for file in files:
    with codecs.open(file,'r','utf-8') as f:
        data.append(json.load(f,encoding='utf-b'))

heroes = [0]*150
wins = [0]*150
winrate = [0]*150

for i in range(0, len(data)):
    print(data[i]['result']['radiant_win'])
    for j in range(0, len(data[i]['result']['picks_bans'])):
        index = data[i]['result']['picks_bans'][j]['hero_id']
        heroes[index] += 1
        if data[i]['result']['radiant_win'] == True and data[i]['result']['picks_bans'][j]['team'] == 0:
            wins[index] += 1
        elif data[i]['result']['radiant_win'] == False and data[i]['result']['picks_bans'][j]['team'] == 1:
            wins[index] += 1
        winrate[index] = wins[index]/heroes[index]
print(winrate)

hfiles = []
for file in os.listdir("C:/Users/Andre/dotaproject/dotadata/dota-oracle/doracle/data"):
    if file.endswith(".json"):
        hfiles.append(os.path.join("C:/Users/Andre/dotaproject/dotadata/dota-oracle/doracle/data", file))
hdata = []

for file in hfiles:
    with codecs.open(file,'r','utf-8') as f:
        hdata.append(json.load(f,encoding='utf-b'))

print(hdata)