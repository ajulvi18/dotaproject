from app import *
import pickle
import glob
import json
import pandas as pd


# get the selected teams and then run through algorithm, have a button that says predict winner
# most likely winner early game most likely winner lategame
def one_hot_encode(teams):
    print(teams)
    x_data = get_file('data/data_frame.pkl')

    if True:
        for item in teams:
            for j in range(len(teams[item])):
                if item == 'radiant':
                    hero_id_1_int = teams['radiant'][j]
                    hero_id_1_str = str(hero_id_1_int)
                    player_1_team = 'radiant'
                    x_data[hero_id_1_str] = 1
                else:
                    hero_id_1_int = teams['dire'][j]
                    hero_id_1_str = str(hero_id_1_int)
                    player_1_team = 'dire'
                    x_data[hero_id_1_str] = -1

                for items in teams:
                    for k in range(len(teams[items])):
                        if items == 'radiant':
                            hero_id_2_int = teams['radiant'][k]
                            hero_id_2_str = str(hero_id_1_int)
                            player_2_team = 'radiant'
                        else:
                            hero_id_2_int = teams['dire'][k]
                            hero_id_2_str = str(hero_id_1_int)
                            player_2_team = 'dire'

                        list = [hero_id_1_int, hero_id_2_int]
                        if player_1_team == player_2_team:
                            if hero_id_1_int == hero_id_2_int:
                                continue
                            if j > k:
                                continue
                            list = [hero_id_1_int, hero_id_2_int]
                            list.sort(reverse=False)
                            index = str(list[0]) + '|' + str(list[1])
                            if index == '10|11':
                                a = 2
                            if player_2_team == 'radiant':
                                x_data[index] = 1

                            elif player_2_team == 'dire':
                                x_data[index] = -1

                        elif player_1_team != player_2_team:
                            index = 'n' + str(list[0]) + '|' + str(list[1])
                            if hero_id_1_str == str(list[0]):
                                list.sort(reverse=False)
                                index = 'n' + str(list[0]) + '|' + str(list[1])
                                x_data[index] = 1
                            elif hero_id_1_int == str(list[1]):
                                list.sort(reverse=False)
                                index = 'n' + str(list[0]) + '|' + str(list[1])
                                x_data[index] = -1

    x_data['late_game'][1] = 1
    x_data['early_game'][0] = 1
    return x_data

def score_tree(rf, x_data, score):
    x_data_early = np.reshape(x_data, (1, 130))
    x_data_early[0][129] = 1

    x_data_late = np.reshape(x_data, (1, 130))
    x_data_late[0][128] = 1

    score_tree_early = int(rf.predict(x_data_early))
    score_tree_late = int(rf.predict(x_data_late))
    score[0][score_tree_early] += 1
    score[1][score_tree_late] += 1
    return score


def get_file(file_path):
    with open(file_path, 'rb') as fp:
        file = pickle.load(fp)
    return file


def get_random_forests():
    pass


def predict_duration(x_values):
    ohe = np.zeros(150)
    for item in x_values:
        for i in range(len(x_values[item])):
            index = x_values[item][i]
            if item == 'radiant':
                ohe[index] = 1
            else:
                ohe[index] = -1
    ohe = np.reshape(ohe, (1, -1))

    # Stochastic gradient descent algorithm
    lr = get_file('data/linear_regression.pkl')
    duration = lr.predict(ohe)
    duration = duration[0]
    return round(duration / 60)


def predict_winning_team(teams):
    x_value = one_hot_encode(teams)
    get_random_forests()
    score = np.zeros((2, 2))
    file_path = 'data/random_forests_1.pkl'
    rff = get_file(file_path)
    d = rff.predict(x_value)
    wins_early = d[0]
    wins_late = d[1]
    # got to do democratic decision first


    winning_team_early = d[0]
    winning_team_late = d[1]

    if winning_team_early == winning_team_late:
        if winning_team_early == 0:
            winning_team = 'Likely winner in both early and late game is team Dire'
        else:
            winning_team = 'Likely winner in both early and late game is team Radiant'
    else:
        if winning_team_early == 0:
            winning_team = 'Likely winner early game: Team Dire (before 30 minutes), '
        elif winning_team_early == 1:
            winning_team = 'Likely winner early game: Team Radiant (before 30 minutes), '
        elif len(winning_team_late) > 1:
            winning_team = 'No determined winner in the early game (before 30 minutes), '
        else:
            winning_team = 'error, '

        if winning_team_late == 0:
            winning_team += 'Likely winner late game: Team Dire (after 30 minutes)'
        elif winning_team_late == 1:
            winning_team += 'Likely winner late game: Team Radiant (after 30 minutes)'
        elif len(winning_team) > 1:
            winning_team += 'No determined winner in the late game (after 30 minutes)'
        else:
            winning_team += 'error'

    duration = predict_duration(teams)
    winning_team += (' and the match is predicted to last ' + str(duration) + ' minutes.')

    # one hot encoding
    ohe = np.zeros(150)
    for item in teams:
        for i in range(len(teams[item])):
            index = teams[item][i]
            if item == 'radiant':
                ohe[index] = 1
            else:
                ohe[index] = -1
    ohe = np.reshape(ohe, (1, -1))

    # Stochastic gradient descent algorithm

    lr = get_file('data/linear_regression_first_blood.pkl')
    first_blood = lr.predict(ohe)
    first_blood = str(round(first_blood[0] / 60))

    winning_team += ' First blood is predicted to occur after ' + first_blood + ' minutes.'

    return winning_team


def predict_kill_death(x_values):
    string = ''
    ohe = np.zeros(150)
    for item in x_values:
        for i in range(len(x_values[item])):
            index = x_values[item][i]
            if item == 'radiant':
                ohe[index] = 1
            else:
                ohe[index] = -1
    ohe = np.reshape(ohe, (1, -1))

    # Stochastic gradient descent algorithm

    lr = get_file('data/kill_death_prediction/radiant_kill_prediction.pkl')
    radiant_kills = lr.predict(ohe)
    radiant_kills = str(round(radiant_kills[0]))

    lr = get_file('data/kill_death_prediction/radiant_death_prediction.pkl')
    radiant_deaths = lr.predict(ohe)
    radiant_deaths = str(round(radiant_deaths[0]))

    lr = get_file('data/kill_death_prediction/dire_kill_prediction.pkl')
    dire_kills = lr.predict(ohe)
    dire_kills = str(round(dire_kills[0]))

    lr = get_file('data/kill_death_prediction/dire_death_prediction.pkl')
    dire_deaths = lr.predict(ohe)
    dire_deaths = str(round(dire_deaths[0]))
    string += 'Team radiant is predicted to have ' + radiant_kills + ' kills and have ' + radiant_deaths + ' deaths.'
    string += ' Team Dire is predicted to have ' + dire_kills + ' kills and have ' + dire_deaths + ' deaths.'
    return string


def get_heroes(file):
    data = []
    for file in glob.glob(file):
        with open(file, 'r', encoding='utf-8') as fp:
            jsonfile = json.load(fp)
        data.append(jsonfile)
    data = np.array(data)
    return data


def find_hero_types(hero):
    hero = hero['radiant'][0]
    hero_types = get_file('data/hero_types.pkl')
    heroes = get_heroes('data/heroes.json')
    co_hero = get_file('data/synergy.pkl')
    adversary = get_file('data/nemesis.pkl')

    heroes = heroes[0]
    for i in range(len(hero_types)):
        if hero == i:
            hero_type = hero_types[i]
            break
    for j in range(len(heroes)):
        if heroes[j]['id'] == hero:
            hero_name = heroes[j]['name']

    hero_2 = co_hero[hero]

    for l in range(len(heroes)):
        if heroes[l]['id'] == hero_2:
            hero_2_name = heroes[l]['name']
    nemesis = adversary[hero][0]
    for n in range(len(heroes)):
        if heroes[n]['id'] == nemesis:
            nemesis_name = heroes[n]['name']

    if hero_type == 1:
        string = hero_name + " is not very reliant on the team mates choices of heroes to perform optimally but performs well with " + hero_2_name + '. The hero may be countered by ' + nemesis_name + '.'
    elif hero_type == 2:
        string = hero_name + ' is reliant on the team mates choices of heroes to perform optimally and performs well with ' + hero_2_name + '. The hero may be countered by ' + nemesis_name + '.'
    elif hero_type == 3:
        string = hero_name + ' is somewhat reliant on the team mates choices of heroes to perform optimally, and performs well with ' + hero_2_name + '. The hero may be countered by ' + nemesis_name + '.'
    else:
        string = 'invalid hero'
    return string

