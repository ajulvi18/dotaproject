import pickle
import numpy as np

from sklearn.ensemble import RandomForestClassifier

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import datetime
import re


def get_data(file):
    with open(file, 'rb') as fp:
        file = pickle.load(fp)
    data = np.array(file)
    return data


def extract_features(data):

    # hero_id and y values
    hero_feature = np.zeros((len(data), 11), dtype=int)
    y_value = np.zeros((len(data)))
    one_hot_encoding = np.zeros((len(data), 130))
    for i in range(len(data)):

        duration = data[i]['result']['duration'] / 60
        if duration <= 30:
            one_hot_encoding[i][128] = 1
        elif duration > 30:
            one_hot_encoding[i][129] = 1

        if 'picks_bans' in data[i]['result']: #dont think this part is important, it was when i was using picks to find the heroes earlier.
            for j in range(len(data[i]['result']['players'])):
                hero_feature[i][j] = data[i]['result']['players'][j]['hero_id']
                if data[i]['result']['players'][j]['player_slot'] < 10:
                    one_hot_encoding[i][hero_feature[i][j]] = 1
                if data[i]['result']['players'][j]['player_slot'] > 10:
                    one_hot_encoding[i][hero_feature[i][j]] = -1
        else:
            continue
        #turn boolean to int
        y_value[i] = int(data[i]['result']['radiant_win'])
    return one_hot_encoding, y_value


def decision_tree(x_data, y_data):
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.20, random_state=15)
    rf = RandomForestClassifier(n_estimators=25, max_depth=20, criterion='gini')

    rf.fit(x_train, y_train)
    print('random forest accuracy training set:', accuracy_score(y_train, rf.predict(x_train)))
    print('random forest accuracy test set:', accuracy_score(y_test, rf.predict(x_test)))
    accuracy = accuracy_score(y_test, rf.predict(x_test))
    return rf, accuracy


def store_random_forests(rff, file_id):
    destination = '/Users/hagtv/Desktop/Studies/høst_2020/IKT110/project/DOTA_Dataset/final_models/predict_outcome_rf/' + file_id + '.pkl'
    with open(destination, 'wb') as fd:
        pickle.dump(rff, fd)


def main():
    r = 0
    rff = []
    for i in range(98):
        file_path = '/Users/hagtv/Desktop/Studies/høst_2020/IKT110/project/DOTA_Dataset/Cleaned_data/cleaned_data'
        print(i)
        data = get_data(file_path)
        data_x, data_y = extract_features(data)
        rf, accuracy = decision_tree(data_x, data_y)
        #append each forest to a list
        rff.append(rf)
    print('a')
    store_random_forests(rff, 'random_forests')


if __name__ == '__main__':
    main()
